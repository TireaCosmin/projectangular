import { isEmptyExpression } from '@angular/compiler';
//import { Component } from '@angular/core';
import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
/*@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})*/
export class AppComponent {
  title = 'todo-app';

  nextId = 4;
  todos = [
    {
      id: 1,
      label: 'Bring Milk',
      done: false
    },
    {
      id: 2,
      label: 'Pay mobile bill',
      done: false
    },
    {
      id: 3,
      label: 'Learn Angular',
      done: false
    },
  ];

  ifEmpty(todolabel) {
    if(todolabel!="" && todolabel!=" " && todolabel!="  " && todolabel!="   ")
      return 1;
    
    return 0;
  }
  delete(todo) {
    this.todos = this.todos.filter(t => t.id !== todo.id);
    this.nextId--;
  }
   
  add(newTodoLabel) {
    let newTodo = {
      id: this.nextId,
      label: newTodoLabel,
      done: false
    };
    if(this.ifEmpty(newTodoLabel)==1)
    {
    this.todos.push(newTodo);
    this.nextId++;
    }
  }

  toggle(todo) {
    todo.done = !todo.done;
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings/settings.component';
import {Routes,RouterModule} from '@angular/router';

const routes: Routes=[
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(n=>n.AdminModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

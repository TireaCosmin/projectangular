import {
    ValidationErrors,
    ValidatorFn,
    AbstractControl,
    FormControl,
} from "@angular/forms";

export class CustomValidators {
    public static ifEmpty(control: AbstractControl) {
        const regExp: RegExp = /^\s+$/;
        return regExp.test(control.value) ? null : { ifEmpty: true };
    }

}